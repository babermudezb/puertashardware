from __future__ import print_function
import BlynkLib

BLYNK_AUTH = 'f9bc63ec578d4d2a8a4d2bd1d93021e8'

# Initialize Blynk
blynk = BlynkLib.Blynk(BLYNK_AUTH,
                       server='blynk.thecloudcomputing.io', # set server address
                       port=8080,              # set server port
                       heartbeat=30,           # set heartbeat to 30 secs
                       log=print               # use print function for debug logging
                       )


@blynk.ON("V*")
def blynk_handle_vpins(pin, value):
    #print("V{} value: {}".format(pin, value))
    #blynk.virtual_write(1, 255)
    print(pin)
    print(value[0])
    if pin == '0':
        if value[0] == '1':
            blynk.virtual_write(1, 255)
        else:
            blynk.virtual_write(1, 0)




while True:
    blynk.run()