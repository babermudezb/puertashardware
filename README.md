# Desarrollo Firmware

En este repositorio estarán los scripts de python para el proyecto final de Aplicaciones Sobre internet - La nube correspondiente a la apertura automática de las puertas de las salas de informática.


## 1. Instalación previa en la raspberry pi 

### Instalación de PIP

para instalar pip se ejecutan lo siguientes comandos: 

```
sudo apt-get update
sudp apt-get upgrade
sudo apt-get install python3-pip

```
### Configuracion de LCD

Se debe verificar la direcciónd e la LCD I2C a utilizar, en este caso es el módulo PCF8574AT cuya dirección i2C es 0x3F, seguido a esto se debe instalar el módulo SMBUS (Por obligación debe estar habilitado el uso del bus I2C lo cual se realiza desde la configuración de la raspberri py con _sudo raspi-config_):

```
sudo apt-get install python-smbus
```


