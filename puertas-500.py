from __future__ import print_function
import BlynkLib
import RPi.GPIO as GPIO
import time
import I2C_LCD_Driver

BLYNK_AUTH = 'f9bc63ec578d4d2a8a4d2bd1d93021e8'


GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.OUT)
GPIO.setup(27, GPIO.OUT)
GPIO.setup(5, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)
GPIO.output(22, True)
time.sleep(2)
GPIO.output(22, False)
mylcd = I2C_LCD_Driver.lcd()

mylcd.lcd_display_string("Sala 500", 1, 4)
mylcd.lcd_display_string("Iniciando", 2, 3)
time.sleep(60)
mylcd.lcd_clear()

mylcd.lcd_display_string("Sala 500", 1, 4)
mylcd.lcd_display_string("Iniciado!", 2, 3)
time.sleep(1)
mylcd.lcd_clear()

mylcd.lcd_display_string("> 500: ", 1, 0)

# Initialize Blynk
blynk = BlynkLib.Blynk(BLYNK_AUTH,
                       server='blynk.thecloudcomputing.io', # set server address
                       port=8080,              # set server port
                       heartbeat=30,           # set heartbeat to 30 secs
                       log=print               # use print function for debug logging
                       )


@blynk.ON("V*")
def blynk_handle_vpins(pin, value):
    #print("V{} value: {}".format(pin, value))
    #blynk.virtual_write(1, 255)
    print(pin)
    print(value[0])
    if pin == '0':
        if value[0] == '1':
            blynk.virtual_write(1, 255)
            GPIO.output(17, False)
            GPIO.output(27, True)
            GPIO.output(5, True)
            GPIO.output(22, True)
            mylcd.lcd_clear()
            mylcd.lcd_display_string("> 500: GRANTED  ", 1, 0)
            mylcd.lcd_display_string("Access By: Admin", 2, 0)
            time.sleep(5)
            blynk.virtual_write(1, 0)
            blynk.virtual_write(0, 0)
            GPIO.output(17, True)
            GPIO.output(27, False)
            GPIO.output(5, False)
            GPIO.output(22, False)
            mylcd.lcd_clear()
            mylcd.lcd_display_string("> 500: LOCKED   ", 1, 0)


while True:
    blynk.run()
